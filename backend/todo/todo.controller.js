import { getAll, create, update, remove } from './todo.model.js';

async function getTodos(request, response) {
  const todos = await getAll();
  response.json(todos);
}

async function createTodo(request, response) {
  const todo = await create(request.body);
  response.json(todo);
}

async function updateTodo(request, response) {
  const todoId = request.params.id;
  await update(todoId, request.body)
    .then((todo) => {
      response.json(todo);
    })
    .catch((error) => {
      response.status(404).json({ message: error });
    });
}

async function removeTodo(request, response) {
  const todoId = request.params.id;
  await remove(todoId)
    .then((todo) => {
      response.json(todo);
    })
    .catch((error) => {
      response.status(404).json({ message: error });
    });
}

export { getTodos, createTodo, updateTodo, removeTodo };
